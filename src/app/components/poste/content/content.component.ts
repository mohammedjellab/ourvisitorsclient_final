import { Component, OnInit } from '@angular/core';
import { VisiteurService } from 'src/app/core/services/visiteur/visiteur.service';
import { MatTableDataSource, MatAutocomplete } from '@angular/material';
import { FormGroup, FormBuilder, NgForm, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Societe } from 'src/app/shared/models/societe';
import { SocieteService } from 'src/app/core/services/societe/societe.service';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  posteForm: FormGroup;
  id: 0;
  nomComplet: '';
  cinCnss: '';
  personneService: '';
  telephone: '';
  numBadge: 0;
  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['nomComplet', 'cinCnss', 'personneService', 'heureEntree', 'heureSortie', 'telephone', 'Societe', 'numBadge', 'actions'];
  datasource;

  noms = [];
  societes = [];
  filteredSocietes: Observable<any[]>;
  filteredNoms: Observable<any[]>;
  societeControl = new FormControl();
  nomControl = new FormControl();

  constructor(private servSociete: SocieteService,
              private formBuilder: FormBuilder,
              private service: VisiteurService,
              private router: Router) { }

  ngOnInit() {
    this.filterInitSociete();
    this.filterInitNom();
    this.showConfig();
    this.showSociete();
    this.showNom();
    this.posteForm = this.formBuilder.group({
      Societe: this.societeControl,
      nomComplet: this.nomControl,
      cinCnss: [null, [Validators.required]],
      personneService: [null, [Validators.required]],
      // 'heureEntree': [null, [Validators.required]],
      // 'heureSortie': [null, [Validators.required]],
      // 'dateVisite': [null, [Validators.required]],
      telephone: [null],
      numBadge: [null],
    });
  }
  resetform() {
    this.posteForm.reset();
  }
  showConfig() {
    this.service.getVisitorsToday()
      .subscribe((data: any[]) => {
        this.datasource = new MatTableDataSource(data);
      });
  }

  showSociete() {
    this.servSociete.getSocietes()
      .subscribe((data: any[]) => {
        console.log(data);
        this.societes = data;
      });
  }
  showNom(){
    this.service.getAllVisitors().subscribe((data : any[])=>{
      console.log(data);
      this.noms = data;
    })
  }

  onFormSubmit(form: NgForm) {
    this.service.addVisitor(form).subscribe(res => {
      this.resetform();
      this.showConfig();

      console.log(form);
    },
      err => console.log('error'));

    this.router.navigate(['poste/regle']);
  }

  private filterInitSociete() {
    this.filteredSocietes = this.societeControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterSociete(value))
      );
  }

  private _filterSociete(value: string): any[] {
    const filterValue = value != null ? value.toLowerCase() : '';
    return this.societes.filter(e => e.nomSociete.toLowerCase().includes(filterValue));
  }

  private filterInitNom() {
    this.filteredNoms = this.nomControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterNom(value))
      );
  }

  private _filterNom(value: string): any[] {
    const filterValue = value != null ? value.toLowerCase() : '';
    return this.noms.filter(e => e.nomComplet.toLowerCase().includes(filterValue));
  }

  OnSortie(id) {
    if (confirm('Validez la sortie ?')) {
      console.log('Sortie ID: ', id);
      this.service.sortieVisiteur(id).subscribe(res => {
        this.showConfig();
        console.log('Sortie Ok', res);
      },
        err => console.log(err));
    }
  }

  getVisitor(nom){
    this.posteForm.controls.cinCnss.patchValue(nom.cinCnss);
    this.posteForm.controls.personneService.patchValue(nom.personneService);
    this.posteForm.controls.telephone.patchValue(nom.telephone);
    this.posteForm.controls.Societe.patchValue(nom.idSocieteNavigation.nomSociete);
    this.posteForm.controls.numBadge.patchValue(nom.numBadge);
  }

}
