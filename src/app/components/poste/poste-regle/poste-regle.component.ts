import { NgbCarouselConfig, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RegleService } from 'src/app/core/services/regle/regle.service';
import { constants } from 'src/app/shared/constants';
import * as $ from 'jquery';
@Component({
  selector: 'app-poste-regle',
  templateUrl: './poste-regle.component.html',
  styleUrls: ['./poste-regle.component.scss'],
  providers: [NgbCarouselConfig],
  encapsulation: ViewEncapsulation.None
})
export class PosteRegleComponent implements OnInit {

  imageServer;
  imageLength = 0;
  imageToShow = [];

  constructor(private regleService: RegleService, config: NgbCarouselConfig) {
    config.interval = null;
    config.wrap = false;
  }

  ngOnInit() {
    this.regleService.getRulesByOrder().subscribe((res: any[]) => {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < res.length; i++) {
        if (res[i].show) {
          this.imageToShow.push(res[i]);
          this.imageLength++;
        }
      }
      this.imageServer = res;
      console.log(this.imageLength);
    });
  }

  onSlide(slideEvent: NgbSlideEvent) {
    console.log('Events: ', slideEvent);
    if (slideEvent.source === NgbSlideEventSource.ARROW_RIGHT) {
      const imageIndex = parseInt(slideEvent.current.replace("slideId_", ""), 10);
      console.log('Index: ', imageIndex);
      console.log('ImageLength: ', this.imageLength);
      if (imageIndex === this.imageLength - 1) {
        // console.log($('div.carousel-item active'));
        // console.log($('ngb-carousel a.carousel-control-next span.carousel-control-next-icon'));
        $('ngb-carousel a.carousel-control-next span.carousel-control-next-icon').css({ 'background-image': 'none' });
        // console.log('Is Enter');
      } else {
        $('ngb-carousel a.carousel-control-next span.carousel-control-next-icon').css({ 'background-image': "url('./assets/images/suivant.svg')" });
      }
    } else {
      $('ngb-carousel a.carousel-control-next span.carousel-control-next-icon').css({ 'background-image': "url('./assets/images/suivant.svg')" });

    }

  }

  createImagePath(serverPath: string) {
    return `${constants.serverImg}${serverPath}`;
    // return `http://localhost:4772/${serverPath}`;
  }
}
